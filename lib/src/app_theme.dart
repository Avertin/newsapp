import 'package:flutter/material.dart';

class AppTheme {

  AppTheme._();

  static const Color _lightPrimaryColor = Colors.white; 
  static const Color _lightOnPrimaryColor = Colors.black;
  static const Color _lightMutedOnPrimaryColor = Color(0xFF828282);
  static const Color _lightOnPrimaryArticleTitleColor = Color(0xFF333333);
  static const Color _lightPrimaryVariantColor = Color(0xFFF7F7F7);
  static const Color _lightSecondaryColor = Color(0xFF1A1A1A); 
  static const Color _lightSecondaryVariantColor = Color.fromRGBO(187, 51, 55, 0.3); 
  static const Color _lightOnSecondaryIconColor = Colors.white;

  static final ThemeData lightTheme = ThemeData(

    colorScheme: ColorScheme.light(
      primary: _lightPrimaryColor, 
      primaryVariant: _lightPrimaryVariantColor, 
      secondary: _lightSecondaryColor, 
      secondaryVariant: _lightSecondaryVariantColor, 
    ), 

    iconTheme: IconThemeData(
      color: _lightOnSecondaryIconColor,
    ),   

    visualDensity: VisualDensity.adaptivePlatformDensity,

    textTheme: _lightTextTheme,    

    // inputDecorationTheme: _lightInputDecorationTheme,

  );

  static final TextTheme _lightTextTheme = TextTheme  (
    headline1:  _lightOnPrimaryScreenHeadingTextStyle,
    caption: _lightOnPrimaryCaptionTextStyle,
    headline6: _lightOnPrimaryArticleTitleTextStyle, //Article Title 
  );

  static final TextStyle _lightOnPrimaryScreenHeadingTextStyle = TextStyle(fontSize: 20.0, color: _lightOnPrimaryColor, letterSpacing: 1, fontWeight: FontWeight.w500);
  static final TextStyle _lightOnPrimaryArticleTitleTextStyle = TextStyle(fontSize: 16.0, color: _lightOnPrimaryArticleTitleColor, letterSpacing: 0, fontWeight: FontWeight.bold);
  static final TextStyle _lightOnPrimaryCaptionTextStyle = TextStyle(fontSize: 12.0, color: _lightMutedOnPrimaryColor, fontWeight: FontWeight.normal);

  // static final InputDecorationTheme _lightInputDecorationTheme  = InputDecorationTheme(
  //   alignLabelWithHint: false,
  //   labelStyle: _lightScreenlabelTextMutedStyle,
  //   hintStyle: TextStyle(fontSize: 14),
  //   focusedBorder: UnderlineInputBorder(borderSide: BorderSide(width: 0.2, color: _lightOnPrimaryMutedColor),),
  // ); 

  // static final TextStyle _lightScreenlabelTextMutedStyle = TextStyle(fontSize: 14.0, color: _lightOnPrimaryMutedColor, fontWeight: FontWeight.w500);

} 