// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i2;

import 'ui/groupe_screens/group_screen.dart' as _i5;
import 'ui/groupe_screens/news_list.dart' as _i6;
import 'ui/groupe_screens/tab2.dart' as _i7;
import 'ui/groupe_screens/tab3.dart' as _i8;
import 'ui/news_detail.dart' as _i4;
import 'ui/splashscreen.dart' as _i3;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState> navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    SplashScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i3.SplashScreen();
        }),
    NewsDetailRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<NewsDetailRouteArgs>(
              orElse: () => const NewsDetailRouteArgs());
          return _i4.NewsDetail(
              title: args.title,
              urlToImage: args.urlToImage,
              publishedAt: args.publishedAt,
              content: args.content,
              author: args.author);
        }),
    GroupScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final pathParams = data.pathParams;
          final args = data.argsAs<GroupScreenRouteArgs>(
              orElse: () =>
                  GroupScreenRouteArgs(id: pathParams.getString('id')));
          return _i5.GroupScreen(id: args.id);
        }),
    GroupTab1Router.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i1.EmptyRouterPage();
        }),
    GroupTab2Router.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i1.EmptyRouterPage();
        }),
    GroupTab3Router.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i1.EmptyRouterPage();
        }),
    NewsListRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i6.NewsList();
        }),
    Tab2ScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i7.Tab2Screen();
        }),
    Tab3ScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i8.Tab3Screen();
        })
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig('/#redirect',
            path: '/', redirectTo: 'splash', fullMatch: true),
        _i1.RouteConfig(SplashScreenRoute.name, path: 'splash'),
        _i1.RouteConfig(NewsDetailRoute.name, path: 'newsdetails'),
        _i1.RouteConfig(GroupScreenRoute.name, path: 'group/:id', children: [
          _i1.RouteConfig(GroupTab1Router.name, path: 'tab1', children: [
            _i1.RouteConfig(NewsListRoute.name, path: ''),
            _i1.RouteConfig('*#redirect',
                path: '*', redirectTo: '', fullMatch: true)
          ]),
          _i1.RouteConfig(GroupTab2Router.name, path: 'tab2', children: [
            _i1.RouteConfig(Tab2ScreenRoute.name, path: ''),
            _i1.RouteConfig('*#redirect',
                path: '*', redirectTo: '', fullMatch: true)
          ]),
          _i1.RouteConfig(GroupTab3Router.name, path: 'tab3', children: [
            _i1.RouteConfig(Tab3ScreenRoute.name, path: ''),
            _i1.RouteConfig('*#redirect',
                path: '*', redirectTo: '', fullMatch: true)
          ])
        ])
      ];
}

class SplashScreenRoute extends _i1.PageRouteInfo {
  const SplashScreenRoute() : super(name, path: 'splash');

  static const String name = 'SplashScreenRoute';
}

class NewsDetailRoute extends _i1.PageRouteInfo<NewsDetailRouteArgs> {
  NewsDetailRoute(
      {String title,
      String urlToImage,
      String publishedAt,
      dynamic content,
      String author})
      : super(name,
            path: 'newsdetails',
            args: NewsDetailRouteArgs(
                title: title,
                urlToImage: urlToImage,
                publishedAt: publishedAt,
                content: content,
                author: author));

  static const String name = 'NewsDetailRoute';
}

class NewsDetailRouteArgs {
  const NewsDetailRouteArgs(
      {this.title,
      this.urlToImage,
      this.publishedAt,
      this.content,
      this.author});

  final String title;

  final String urlToImage;

  final String publishedAt;

  final dynamic content;

  final String author;
}

class GroupScreenRoute extends _i1.PageRouteInfo<GroupScreenRouteArgs> {
  GroupScreenRoute({String id, List<_i1.PageRouteInfo> children})
      : super(name,
            path: 'group/:id',
            args: GroupScreenRouteArgs(id: id),
            rawPathParams: {'id': id},
            initialChildren: children);

  static const String name = 'GroupScreenRoute';
}

class GroupScreenRouteArgs {
  const GroupScreenRouteArgs({this.id});

  final String id;
}

class GroupTab1Router extends _i1.PageRouteInfo {
  const GroupTab1Router({List<_i1.PageRouteInfo> children})
      : super(name, path: 'tab1', initialChildren: children);

  static const String name = 'GroupTab1Router';
}

class GroupTab2Router extends _i1.PageRouteInfo {
  const GroupTab2Router({List<_i1.PageRouteInfo> children})
      : super(name, path: 'tab2', initialChildren: children);

  static const String name = 'GroupTab2Router';
}

class GroupTab3Router extends _i1.PageRouteInfo {
  const GroupTab3Router({List<_i1.PageRouteInfo> children})
      : super(name, path: 'tab3', initialChildren: children);

  static const String name = 'GroupTab3Router';
}

class NewsListRoute extends _i1.PageRouteInfo {
  const NewsListRoute() : super(name, path: '');

  static const String name = 'NewsListRoute';
}

class Tab2ScreenRoute extends _i1.PageRouteInfo {
  const Tab2ScreenRoute() : super(name, path: '');

  static const String name = 'Tab2ScreenRoute';
}

class Tab3ScreenRoute extends _i1.PageRouteInfo {
  const Tab3ScreenRoute() : super(name, path: '');

  static const String name = 'Tab3ScreenRoute';
}
