import './news_api_provider.dart';
import '../models/headline_model.dart';
import 'dart:async';

class Repository {
  final newsApiProvider = NewsApiProvider();

  Future<HeadlineModel> fetchAllHeadlines() {
    return newsApiProvider.fetchTopHeadlinesList();
  }

  Future<HeadlineModel> fetchAllTechHeadlines() {
    return newsApiProvider.fetchTopTechHeadlinesList();
  }
}