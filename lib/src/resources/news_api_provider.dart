import 'dart:async';
import 'package:http/http.dart' show Client;
import 'dart:convert';
import '../models/headline_model.dart';

class NewsApiProvider {
  Client client = new Client();
  final String baseUrl = 'https://newsapi.org/v2/top-headlines';
  final String apiKey = '5122fdcc41ba4c26b5b38176583954a9'; 
  final String country = 'fr';
  final String category = 'technology';
  

  Future<HeadlineModel> fetchTopHeadlinesList() async {
    print("entered");
    final response = await client.get(Uri.parse('$baseUrl?country=$country&apiKey=5122fdcc41ba4c26b5b38176583954a9'));
    // final response = await client.get(Uri.parse('$baseUrl'), headers: {'x-api-key': '$apiKey', 'country': '$country', 'category': 'business'});
    print(response.body.toString());
    if (response.statusCode == 200){
      return HeadlineModel.fromJson(jsonDecode(response.body));
    }
    else {
      throw Exception('Erreur lors de la recuperation des donnees');
    }
  }

  Future<HeadlineModel> fetchTopTechHeadlinesList() async {
    print("entered");
    final response = await client.get(Uri.parse('$baseUrl?language=fr&country=fr&category=$category&apiKey=5122fdcc41ba4c26b5b38176583954a9'));
    print(response.body.toString());
    if (response.statusCode == 200){
      return HeadlineModel.fromJson(jsonDecode(response.body));
    }
    else {
      throw Exception('Erreur lors de la recuperation des donnees');
    }
  }
}