import 'package:flutter/material.dart';
import 'package:newsapp/src/routes.gr.dart';
import 'app_theme.dart';

class App extends StatelessWidget {
  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: 'News App',
      theme: AppTheme.lightTheme,
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),  
      builder: (context, router) => router,
    );

    // return MaterialApp(
    //   debugShowCheckedModeBanner: false,
    //   title: 'News App',
    //   theme: AppTheme.lightTheme,
    //   home: Scaffold(
    //     body: NewsList(),
    //   ),
    // );
  }
}