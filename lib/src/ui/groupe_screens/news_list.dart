import 'package:flutter/material.dart';
import '../shimmer_news_list.dart';
import '../tech_news_carousel.dart';
import '../../blocs/news_bloc.dart';
import '../../models/headline_model.dart';
import '../../blocs/tech_news_bloc.dart';
import 'package:auto_route/auto_route.dart';
import '../../routes.gr.dart';
import '../shimmer_carousel.dart';

class NewsList extends StatefulWidget {
  @override
  _NewsListState createState() => _NewsListState(); 
}

class _NewsListState extends State<NewsList> {

  @override
  void initState() { 
    super.initState();
    bloc.fetchAllHeadlines();
    technewsbloc.fetchAllTechHeadlines();
  }

  @override
  void dispose() { 
    bloc.dispose();
    technewsbloc.dispose();
    super.dispose();
  }

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 16),
                    child: RichText(
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        text: 'Actualité tech',
                        style: Theme.of(context).textTheme.headline1,
                        children: <TextSpan>[
                          TextSpan(text: '.', style: TextStyle(fontSize: 44)),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  StreamBuilder(
                    stream: technewsbloc.allTechHeadlines,
                    builder: (context, AsyncSnapshot<HeadlineModel> snapshot) {
                      if (snapshot.hasData) {
                        return TechNewsCarousel(snapshot: snapshot);
                      } else if (snapshot.hasError) {
                        return Text(snapshot.error.toString());
                      }
                      return ShimmerCarousel();
                    },
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: RichText(
                overflow: TextOverflow.ellipsis,
                text: TextSpan(
                  text: 'A la une',
                  style: Theme.of(context).textTheme.headline1,
                  children: <TextSpan>[
                    TextSpan(text: '.', style: TextStyle(fontSize: 44)),
                  ],
                ),
              ),
            ),
            Expanded(
              child: StreamBuilder(
              stream: bloc.allHeadlines,
              builder: (context, AsyncSnapshot<HeadlineModel> snapshot) {
                if (snapshot.hasData) {
                  return buildList(snapshot);
                } else if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }
                return ShimmerNewsList();
              },
            ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildList(AsyncSnapshot<HeadlineModel> snapshot) {
    return ListView.builder(
      physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      itemCount: snapshot.data.articles.length,
      scrollDirection: Axis.vertical,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          enableFeedback: true,
          onTap: () => openDetailPage(snapshot.data, index),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5.0), 
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 90,
                  height: 90,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    image: DecorationImage(
                      image: snapshot.data.articles[index].urlToImage == null ? AssetImage('assets/images/default.jpg') : NetworkImage(snapshot.data.articles[index].urlToImage),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  width: 225,
                  height: 90,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(snapshot.data.articles[index].title != null ? snapshot.data.articles[index].title : '', style: Theme.of(context).textTheme.headline6, overflow: TextOverflow.ellipsis, maxLines: 2,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(snapshot.data.articles[index].source.name != null ? snapshot.data.articles[index].source.name : '', style: Theme.of(context).textTheme.caption, overflow: TextOverflow.ellipsis,),
                          Text(snapshot.data.articles[index].publishedAt != null ? snapshot.data.articles[index].publishedAt.substring(0, 10) : 'Il y a 15 minutes', style: Theme.of(context).textTheme.caption, overflow: TextOverflow.clip,),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
      );
    });
  }

  openDetailPage(HeadlineModel data, int index) {
    context.router.push(
      NewsDetailRoute(
        title: data.articles[index].title,
        urlToImage: data.articles[index].urlToImage,
        publishedAt: data.articles[index].publishedAt,
        content: data.articles[index].content,
        author: data.articles[index].author,
      )
    ); 
  }
}

