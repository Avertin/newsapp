import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerNewsList extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 6,
      scrollDirection: Axis.vertical,
      itemBuilder: (BuildContext context, int index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5.0), 
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 90,
                  height: 90,
                  color: Colors.white,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  width: 225,
                  height: 90,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: [
                          Container(width: double.infinity, height: 8, color: Colors.white,),
                          Container(width: double.infinity, height: 8, color: Colors.white,),
                        ]
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(width: 20, height: 8, color: Colors.white,),
                          Container(width: 20, height: 8, color: Colors.white,),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            
      ),
        );
    });
  }

}