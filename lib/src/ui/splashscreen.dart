import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:newsapp/src/routes.gr.dart';
import 'dart:async';
import 'package:lottie/lottie.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() { 
    super.initState();
    startTime();    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              width: 300,
              height: 100,
              child: Lottie.asset('assets/splashanime.json'),
            ),
            Text('your news app', style: TextStyle(fontSize: 28, fontWeight: FontWeight.w900),),
          ],
        ),
      ),
      
    );
  }

  startTime() {
    Duration dur = new Duration(milliseconds: 3500);
    return Timer(dur, showNewsListPage);
  }

  showNewsListPage() {
    context.router.replace(GroupScreenRoute(id: '1'));
  }

}