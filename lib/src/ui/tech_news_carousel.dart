import 'package:flutter/material.dart';
import '../models/headline_model.dart';
import 'package:carousel_slider/carousel_slider.dart';

class TechNewsCarousel extends StatelessWidget {
  const TechNewsCarousel({
    Key key,
     this.snapshot,
  }) : super(key: key);

  final AsyncSnapshot<HeadlineModel> snapshot;

  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
      itemCount: snapshot.data.articles.length,
      itemBuilder: (context, index, realIdx) {
        return Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          width: 300,
          // height: 250,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            boxShadow: [BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              offset: Offset(2, 4),
              blurRadius: 2,
              spreadRadius: 7)
            ],
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container( 
                width: 300,
                height: 150,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                    image: _buildImage(snapshot.data.articles[index].urlToImage), 
                    fit: BoxFit.cover
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  snapshot.data.articles[index].title != null ? snapshot.data.articles[index].title : '', 
                  style: Theme.of(context).textTheme.headline6,
                  textAlign: TextAlign.justify,
                  overflow: TextOverflow.clip,
                  maxLines: 2,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: <Widget>[
                          Container(
                            width: 15,
                            height: 15,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(width: 5,),
                          Text(snapshot.data.articles[index].source.name != null ? (snapshot.data.articles[index].source.name.length >= 15 ? snapshot.data.articles[index].source.name.substring(0, 15) : snapshot.data.articles[index].source.name) : 'Source inconnue', style: Theme.of(context).textTheme.caption, overflow: TextOverflow.clip,),
                        ] 
                      ),
                      Text(snapshot.data.articles[index].publishedAt != null ? snapshot.data.articles[index].publishedAt.substring(0, 10) : 'Il y a 15 minutes', style: Theme.of(context).textTheme.caption, overflow: TextOverflow.clip,),
                    ],
                ),
              ),
            ],
          ),
        );
      },
      options: CarouselOptions(
        height: 280,
        aspectRatio: 16/9,
        autoPlay: false,
        enlargeCenterPage: true,
        autoPlayCurve: Curves.fastOutSlowIn,
        enableInfiniteScroll: true,
        autoPlayAnimationDuration: Duration(milliseconds: 1900),
        viewportFraction: 0.8,
      ),
    );
  }

  ImageProvider<Object> _buildImage(String urlTI) {
    if(urlTI == null || urlTI[4] != 's' || urlTI == '') {
      return AssetImage('assets/images/default.jpg');
    }
    else
    {
      return NetworkImage(urlTI);
    }
  }
}