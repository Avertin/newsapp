import 'package:flutter/material.dart';

class NewsDetail extends StatefulWidget {
  final String title;
  final String urlToImage;
  final String publishedAt;
  final content;
  final String author;

  NewsDetail({this.title, this.urlToImage,  this.publishedAt, this.content,  this.author});

  @override
  _NewsDetailState createState() => _NewsDetailState(
    title: title,
    urlToImage: urlToImage,
    publishedAt: publishedAt,
    content: content,
    author: author,
  );
}

class _NewsDetailState extends State<NewsDetail> {
  final String title;
  final String urlToImage;
  final String publishedAt;
  final content;
  final String author;

  _NewsDetailState({this.title,  this.urlToImage,  this.publishedAt, this.content,  this.author});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget> [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: urlToImage[4] != 's' ? AssetImage('assets/images/default.jpg') : NetworkImage(urlToImage),
                  fit: BoxFit.cover,
                ),
              ),
              child: Container(
                color: Color.fromRGBO(0, 0, 0, .1),
                padding: EdgeInsets.only(top: 40, bottom: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          icon: Icon(Icons.arrow_back),
                          onPressed: (){
                            Navigator.pop(context);
                          },
                          color: Colors.white,
                        ),
                        IconButton(
                          icon: Icon(Icons.bookmark_border_outlined),
                          onPressed: (){
                            return null;
                          },
                          color: Colors.white,
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 0.3),
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                            ),
                            padding: EdgeInsets.all(10),
                            child: Text('Technologie', style: TextStyle(color: Colors.white, letterSpacing: 0.3, fontWeight: FontWeight.bold, fontSize: 13),),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Text(title, style: TextStyle(color: Colors.white, fontWeight: FontWeight.w800, fontSize: 18, letterSpacing: 0.2)),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: <Widget>[
                                  Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  SizedBox(width: 5,),
                                  Text(author != null ? (author.length >= 15 ? author.substring(0, 15) : author) : 'Author', style: Theme.of(context).textTheme.caption, overflow: TextOverflow.clip,),
                                ] 
                              ),
                              Text(publishedAt != null ? publishedAt.substring(0, 10) : 'Il y a 15 minutes', style: Theme.of(context).textTheme.caption, overflow: TextOverflow.clip,),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          // Align(
          //   alignment: ,
          // ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
              child: Text(content),
            ),
          ),
        ],
      ),
    );
  }
}


// NestedScrollView(
//           headerSliverBuilder: (BuildContext context,bool innerBoxIsScrolled) {
//             return <Widget>[
//               SliverAppBar(
//                 expandedHeight: 300.0,
//                 floating: false,
//                 pinned: true,
//                 actions: <Widget>[
//                   IconButton(icon: Icon(Icons.bookmark_border_outlined), onPressed: () => null)
//                 ],
//                 elevation: 0.0,
//                 flexibleSpace: FlexibleSpaceBar(
//                   background: Image.network(
//                     urlToImage,
//                     fit: BoxFit.cover,
//                   ),
//                 ),
//               ),
//             ];
//           },
//           body: Container(
//             // decoration: BoxDecoration(
//             //   borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
//             // ),
//             padding: const EdgeInsets.all(10.0),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 Container(margin: EdgeInsets.only(top: 5.0)),
//                 Text(
//                   title,
//                   style: Theme.of(context).textTheme.headline6,
//                   textAlign: TextAlign.justify,
//                 ),
//                 Container(margin: EdgeInsets.only(top: 8.0,
//                     bottom: 8.0)),
//                 Row(
//                   children: <Widget>[
//                     Icon(
//                       Icons.favorite,
//                       color: Colors.red,
//                     ),
//                     Container(
//                       margin: EdgeInsets.only(left: 1.0,
//                           right: 1.0),
//                     ),
//                     Text(
//                       'voteAverage',
//                       style: TextStyle(
//                         fontSize: 18.0,
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.only(left: 10.0,
//                           right: 10.0),
//                     ),
//                     Text(
//                       'releaseDate',
//                       style: TextStyle(
//                         fontSize: 18.0,
//                       ),
//                     ),
//                   ],
//                 ),
//                 Container(margin: EdgeInsets.only(top: 8.0,
//                     bottom: 8.0)),
//                 Text('description'),
//                 Container(margin: EdgeInsets.only(top: 8.0,
//                     bottom: 8.0)),
//                 Text(
//                   "Trailer",
//                   style: TextStyle(
//                     fontSize: 25.0,
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//                 Container(margin: EdgeInsets.only(top: 8.0, bottom: 8.0)),
//               ],
//             ),
//           ),
//         ),