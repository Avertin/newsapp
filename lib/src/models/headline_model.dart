class HeadlineModel {
  String _status = '';
  int _totalResults = 0;
  List<_Article> _articles = [];

  HeadlineModel.fromJson(Map<String, dynamic> parsedJson) {
    _status = parsedJson['status'];
    _totalResults = parsedJson['totalResults'];
    print(parsedJson['articles'].length);
    List<_Article> temp = [];
    for(int i = 0; i < parsedJson['articles'].length; i++) {
      _Article _article = _Article.fromJson(parsedJson['articles'][i]);
      temp.add(_article);
    }
    _articles = temp;
  }

  String get status => _status;
  int get totalResults => _totalResults;
  List<_Article> get articles => _articles;
}

class _Article {
  _Source _source = _Source(id_:'', name_:'');
  String _author = '';
  String _title = '';
  String _description = '';
  String _url = '';
  String _urlToImage = '';
  String _publishedAt = '';
  String _content = '';

  _Article.fromJson(Map<String, dynamic> parsedJson) {
    _source = (parsedJson['source'] != null ? new _Source.fromJson(parsedJson['source']) : null);
    _author = parsedJson['author'];
    _title = parsedJson['title'];
    _description = parsedJson['description'];
    _url = parsedJson['url'];
    _urlToImage = parsedJson['urlToImage'];
    _publishedAt = parsedJson['publishedAt'];
    _content = parsedJson['content'];
  }

  String get author => _author;
  String get title => _title;
  String get description => _description;
  String get url => _url;
  String get urlToImage => _urlToImage;
  String get publishedAt => _publishedAt;
  String get content => _content;
  _Source get source => _source;
} 

class _Source {
  // ignore: non_constant_identifier_names
  String id_ = '';
  // ignore: non_constant_identifier_names
  String name_ = '';

  // ignore: non_constant_identifier_names
  _Source({this.id_, this.name_});

  _Source.fromJson(Map<String, dynamic> parsedJson) {
    id_ = parsedJson['id'];
    name_ = parsedJson['name'];
  }

  // _Source(source){
  //   _id = source['id'];
  //   _name = source['name'];
  // }

  String get id => id_;
  String get name => name_;

}