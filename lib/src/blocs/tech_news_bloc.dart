import '../resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import '../models/headline_model.dart';


class TechNewsNewsBloc {
  final _repository = Repository();
  final _techNewsFetcher = PublishSubject<HeadlineModel>();

  Observable<HeadlineModel> get allTechHeadlines => _techNewsFetcher.stream;

  fetchAllTechHeadlines() async {
    HeadlineModel headlineModel = await _repository.fetchAllTechHeadlines();
    _techNewsFetcher.sink.add(headlineModel);
  }

  void dispose() {
    _techNewsFetcher.close();
  }
}

final technewsbloc = TechNewsNewsBloc();