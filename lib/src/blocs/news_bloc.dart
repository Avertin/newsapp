import '../resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import '../models/headline_model.dart';


class NewsBloc {
  final _repository = Repository();
  final _newsFetcher = PublishSubject<HeadlineModel>();

  Observable<HeadlineModel> get allHeadlines => _newsFetcher.stream;

  fetchAllHeadlines() async {
    HeadlineModel headlineModel = await _repository.fetchAllHeadlines();
    _newsFetcher.sink.add(headlineModel);
  }

  void dispose() {
    _newsFetcher.close();
  }
}

final bloc = NewsBloc();